/**********************************
* Filename: main.cpp
* 
* author: Garo
* created: 2019-01-10
* notes: 
* 
* desc: 
* 
* ver: 2019-01-10 first version
* 
* 
* **********************************/
#include <iostream>
#include <string>
#include <vector>
#include <cstring>
#include <stdlib.h>
#include <cstdio>
#include <cstddef>
#include <sstream>
#include <map>
#include "resistor.hpp"

using namespace std;

int main()
{
    bool choose = true;
    vector<Resistor> res;
    int numberOfResistors;
    int choice;
    string str;
    double sum = 0;
    double minSum = 0;
    double maxSum = 0;
    double tempSum = 0;
    double minT;
    double maxT;
    float tolSum;
    float percentage;
    string num;
    string firstColor;
    string secondColor;
    string thirdColor;
    string forthColor;
    float tol[4] = {0.01, 0.02, 0.05, 0.10};
    string colorDigits[10] = {"Black", "Brown", "Red", "Orange", "Yellow", "Green", "Blue", "Violet", "Grey", "White"};
    string colorMultiplier[10] = {"Black", "Brown", "Red", "Orange", "Yellow", "Green", "Blue", "Violet", "Grey", "White"};
    string colorTolerance[4] = {"Brown", "Red", "Gold", "Silver"};
    float e24[73] = {0.9, 1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0, 3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1,
                     10, 11, 12, 13, 15, 16, 18, 20, 22, 24, 27, 30, 33, 36, 39, 43, 47, 51, 56, 62, 68, 75, 82, 91, 100, 110, 120,
                     130, 150, 160, 180, 200, 220, 240, 270, 300, 330, 360, 390, 430, 470, 510, 560, 620, 680, 750, 820, 910};
    cout << endl;
    cout << endl;
    cout << "This program is capable of calculating values between 100u and 100G. Please do not attemp to calculate values outside of that range! " << endl;
    cout << endl;
    cout << endl;
    cout << "How many resistors you want to connect? " << endl;
    getline(cin, num);
    numberOfResistors = stoi(num);

    for (int i = 1; i <= numberOfResistors; i++)
    {
        string inputAsString = "";
        char symbol = ' ';
        string toleranceValue;
        double tolerance;
        double number = 0;
        cout << "input the resistant of resistor nr " << i << ": " << endl;
        getline(cin, inputAsString);

        size_t found = inputAsString.find_first_of("GMkMUgmku");
        if (found != string::npos)
            symbol = inputAsString[found];

        char input[inputAsString.size() + 1];
        strcpy(input, inputAsString.c_str());
        number = strtod(input, NULL);

        cout << "Choose tolerance value between  1 and 10 percent" << endl;
        getline(cin, toleranceValue);
        tolerance = stod(toleranceValue) / 100;

        if (symbol == 'G' || symbol == 'g')
            number = number * 1000000000;
        else if (symbol == 'M')
            number = number * 1000000;
        else if (symbol == 'k' || symbol == 'K')
            number = number * 1000;
        else if (symbol == 'm')
            number = number / 1000;
        else if (symbol == 'u' || symbol == 'U')
            number = number / 1000000;
        else
            number = number;

        minT = number - (number * tolerance);
        maxT = number + (number * tolerance);

        res.push_back(Resistor(number, minT, maxT));
    }

    while (choose)
    {
        cout << "for serial connection press 1. for paralell connection press 2 " << endl;
        cin >> choice;

        switch (choice)
        {
        case 1:
            for (int i = 0; i < numberOfResistors; i++)
                sum += res[i].get_resistant();
            for (int i = 0; i < numberOfResistors; i++)
                minSum += res[i].get_minTolerance();
            for (int i = 0; i < numberOfResistors; i++)
                maxSum += res[i].get_maxTolerance();
            choose = false;
            break;
        case 2:
            for (int i = 0; i < numberOfResistors; i++)
            {
                double resistant = 1.0 / res[i].get_resistant();
                tempSum += resistant;
                sum = 1.0 / tempSum;
            }
            tempSum = 0.0;
            for (int i = 0; i < numberOfResistors; i++)
            {
                double resistant = 1.0 / res[i].get_minTolerance();
                tempSum += resistant;
                minSum = 1.0 / tempSum;
            }
            tempSum = 0.0;
            for (int i = 0; i < numberOfResistors; i++)
            {
                double resistant = 1.0 / res[i].get_maxTolerance();
                tempSum += resistant;
                maxSum = 1.0 / tempSum;
            }
            choose = false;
            break;
        default:
            cout << "Not a valid choice!" << endl;
            cout << endl;
        }
    }

    string prefix;
    if (sum >= 1000000000000)
    {
        sum = sum / 1000000000000;
        prefix = "TΩ";
    }
    else if (sum >= 1000000000 && sum < 1000000000000)
    {
        sum = sum / 1000000000;
        prefix = "GΩ";
    }
    else if (sum >= 1000000 && sum < 1000000000)
    {
        sum = sum / 1000000;
        prefix = "MΩ";
    }
    else if (sum >= 10000 && sum < 1000000)
    {
        sum = sum / 1000;
        prefix = "kΩ";
    }
    else if (sum >= 1000 && sum < 10000)
    {
        sum = sum / 1000;
        prefix = "kΩ";
    }
    else if (sum > 0.0000009 && sum < 0.001)
    {
        sum = sum * 1000000;
        prefix = "uΩ";
    }
    else if (sum > 0.0009 && sum < 1)
    {
        sum = sum * 1000;
        prefix = "mΩ";
    }
    else
        prefix = "Ω";

    cout << endl; cout << endl; cout << endl;
    cout << "R tot is: ";
    printf("%.2lf", sum);
    cout << " [";
    printf("%.2lf, %.2lf", minSum, maxSum);
    cout << "] " << prefix << endl;
    
    tolSum = minSum/sum;
    tolSum = 1.0 - tolSum;

    
    for (int i = 0; i < 73; i++)
    {
        if (sum == e24[i])
        {
            cout << "The E24 value is: " << e24[i] << prefix << endl;
            
            for (int i = 0; i < 5; i++)
            {
                if (tolSum > tol[i] && tolSum < tol[i + 1])
                {
                    float temp = tol[i] * 100;
                    forthColor = colorTolerance[i];
                    cout << "The recommended tolerance percentage is: " << temp << "%" << endl;
                }
            }
            ostringstream strs;
            strs << e24[i];
            string sumBeforePoint = strs.str();
            int numOfChars = sumBeforePoint.length();
            size_t found = sumBeforePoint.find_first_of(".");
            string sumAfterPoint;
            if (found != string::npos)
            {
                sumAfterPoint = sumBeforePoint.substr(found);
                sumBeforePoint = sumBeforePoint.substr(0, found);
            }
            numOfChars = sumBeforePoint.length();
            if (numOfChars == 1)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "Ω")
                    thirdColor = "Gold";
                else if (prefix == "kΩ")
                    thirdColor = "Red";
                else if (prefix == "MΩ")
                    thirdColor = "Green";
                else
                    thirdColor = colorMultiplier[numOfChars - 1];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                if (sumAfterPoint.length() > 1)
                    secondColor = colorDigits[sumAfterPoint[1] - '0'];
                else
                    secondColor = colorDigits[0];
            }
            if (numOfChars == 2)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "kΩ")
                    thirdColor = "Orange";
                else
                    thirdColor = colorMultiplier[numOfChars - 2];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            if (numOfChars == 3)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "kΩ")
                    thirdColor = "Yellow";
                else
                    thirdColor = colorMultiplier[numOfChars - 2];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            if (numOfChars >= 4 && numOfChars <= 10)
            {
                thirdColor = colorMultiplier[numOfChars - 1];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            cout << "Color map: " << firstColor << ", " << secondColor << ", " << thirdColor << ", " << forthColor << endl;
        }
        else if (sum > e24[i] && sum < e24[i + 1])
        {
            cout << "The recommended E24 value is: " << e24[i + 1] << prefix << endl;
            for (int i = 0; i < 5; i++)
            {
                if (tolSum > tol[i] && tolSum < tol[i + 1])
                {
                    float temp = tol[i] * 100;
                    forthColor = colorTolerance[i];
                    cout << "The recommended tolerance percentage is: " << temp << "%" << endl;
                }
            }
            ostringstream strs;
            strs << e24[i + 1];
            string sumBeforePoint = strs.str();
            int numOfChars = sumBeforePoint.length();
            size_t found = sumBeforePoint.find_first_of(".");
            string sumAfterPoint;
            if (found != string::npos)
            {
                sumAfterPoint = sumBeforePoint.substr(found);
                sumBeforePoint = sumBeforePoint.substr(0, found);
            }
            numOfChars = sumBeforePoint.length();

            if (numOfChars == 1)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "Ω")
                    thirdColor = "Gold";
                else if (prefix == "kΩ")
                    thirdColor = "Red";
                else if (prefix == "MΩ")
                    thirdColor = "Green";
                else
                    thirdColor = colorMultiplier[numOfChars - 1];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                if (sumAfterPoint.length() > 1)
                    secondColor = colorDigits[sumAfterPoint[1] - '0'];
                else
                    secondColor = colorDigits[0];
            }
            if (numOfChars == 2)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "kΩ")
                    thirdColor = "Orange";
                else
                    thirdColor = colorMultiplier[numOfChars - 2];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            if (numOfChars == 3)
            {
                if (prefix == "mΩ")
                    thirdColor = "Silver";
                else if (prefix == "kΩ")
                    thirdColor = "Yellow";
                else
                    thirdColor = colorMultiplier[numOfChars - 2];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            if (numOfChars >= 4 && numOfChars <= 10)
            {
                thirdColor = colorMultiplier[numOfChars - 1];
                firstColor = colorDigits[sumBeforePoint[0] - '0'];
                secondColor = colorDigits[sumBeforePoint[1] - '0'];
            }
            cout << "Color map: " << firstColor << ", " << secondColor << ", " << thirdColor << ", " << forthColor <<  endl;
        }
    }

    return 0;
}
