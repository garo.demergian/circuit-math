#include <iostream>
#include <vector>

#include "resistor.hpp"

using namespace std;

Resistor::Resistor(double value, double minT, double maxT) 
{ m_resistant = value;  minTolerance = minT; maxTolerance = maxT;}
Resistor::Resistor() { m_resistant = 0.0; }
void Resistor::set_minTolerance(double value) {minTolerance = value;}
double Resistor::get_minTolerance() {return minTolerance;}
void Resistor::set_maxTolerance(double value) {maxTolerance = value;}
double Resistor::get_maxTolerance() {return maxTolerance;}
void Resistor::set_resistant(double value) { m_resistant = value; }
double Resistor::get_resistant() { return m_resistant; }
void Resistor::Print() {cout << "The total resistant is: " << m_resistant << endl;}
