#ifndef _RESISTOR_HPP_
#define _RESISTOR_HPP_

#include <vector>
using namespace std;

class Resistor
{
    private:
        double m_resistant;
        double minTolerance;
        double maxTolerance;
    public:
        Resistor(double value, double minT, double macT);
        Resistor();
        void set_minTolerance(double value);
        double get_minTolerance();
        void set_maxTolerance(double value);
        double get_maxTolerance();
        void set_resistant(double value);
        double get_resistant();
        void Print();
};

#endif // _RESISTOR_HPP_
